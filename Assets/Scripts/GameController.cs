﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Runtime.InteropServices.ComTypes;
using System;

public enum EstadoPartida { Parado, Jugando, Acabado, Grande, Envenenado };
public class GameController : MonoBehaviour
{
    //Interfaz
    public GameObject interfazIdle;
    public GameObject interfazMuerte;
    public GameObject interfazSubeNivel;
    public GameObject interfazPuntos;

    //Objetos
    public GameObject jugador;
    public GameObject generadorEnemigos;
    public RawImage fondo;
    public RawImage plataforma;

    //Audio
    private AudioSource musica;

    //Variables
    public EstadoPartida estadoPartida = EstadoPartida.Parado;
    public float segundosCambioDificultad;
    public float segundosEscalaTiempoInc;
    public float velocidadParallax;
    public float tiempoInicial;
    public float tiempoDificultadPrevia;
    private int puntos;
    private bool nuevoRecord;
    public Text puntosTexto;
    public Text puntosRecordTexto;
    public Text nuevoRecordTexto;


    // Start is called before the first frame update
    void Start()
    {
        puntosRecordTexto.text = "BEST: " + GetMaximaPuntuacion().ToString();
        tiempoInicial = Time.timeScale;
        tiempoDificultadPrevia = Time.timeScale;
        musica = GetComponent<AudioSource>();
        interfazMuerte.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        bool UsuarioComienza = Input.GetKeyDown("up") || Input.GetMouseButtonDown(0);

        //Comienza partida
        if (estadoPartida == EstadoPartida.Parado && UsuarioComienza)
        {
            InicioPartida();
        }
        //Partida en juego
        else if (estadoPartida == EstadoPartida.Jugando)
        {
            jugador.gameObject.tag = "Jugador";
            Parallax();
        }
        //Preparacion reinicio y reinicio.
        else if (estadoPartida == EstadoPartida.Acabado)
        {
            MuerteJugador();
            if (UsuarioComienza)
            {
                ReiniciarPartida();
            }
        }
        else if (estadoPartida == EstadoPartida.Grande)
        {
            jugador.gameObject.tag = "JugadorGrande";
            Parallax();
        }
        else if (estadoPartida == EstadoPartida.Envenenado)
        {
            ParallaxEnvenenado();        
        }
    }

    //Tiempo, dificultad y partida
    #region Metodos de acción
    public void ReiniciarPartida()
    {
        SceneManager.LoadScene("JUEGO");
    }

    //Método continuamente llamado por el comienzo de la partida.
    void IncrementaDificultad()
    {
        if (Time.timeScale > 2)
        {
            segundosEscalaTiempoInc /= 2f;
        }
        Time.timeScale += segundosEscalaTiempoInc;
        tiempoDificultadPrevia = Time.timeScale;
        interfazSubeNivel.SetActive(true);
        Invoke("DesactivarInterfaz", 3f);
    }

    void MuerteJugador()
    {
        CancelInvoke("IncrementaDificultad");
        Time.timeScale = tiempoInicial;
        interfazSubeNivel.SetActive(false);
        interfazPuntos.SetActive(false);
        interfazMuerte.SetActive(true);
        if (nuevoRecord)
        {
            nuevoRecordTexto.text = "¡¡¡ NUEVO RECORD : " + GetMaximaPuntuacion().ToString() + " !!!";
            nuevoRecordTexto.enabled = true;
        }
        else
        {
            nuevoRecordTexto.text = string.Empty;
        }
    }

    void InicioPartida()
    {
        nuevoRecord = false;
        nuevoRecordTexto.enabled = false;
        estadoPartida = EstadoPartida.Jugando;
        interfazIdle.SetActive(false);
        interfazPuntos.SetActive(true);
        jugador.SendMessage("ActualizaEstado", "JugadorCorre");
        generadorEnemigos.SendMessage("StartGenerator");
        musica.Play();

        InvokeRepeating("IncrementaDificultad", segundosCambioDificultad, segundosCambioDificultad);
    }

    void RalentizaTiempo()
    {
        Time.timeScale = 0.1f;
    }

    void NormalizaTiempo()
    {
        Time.timeScale = tiempoDificultadPrevia;
    }
    #endregion

    //Puntuacion y parallax
    #region Metodos
    //Llamado segundos despues de activarla en IncrementaDificultad
    void DesactivarInterfaz()
    {
        interfazSubeNivel.SetActive(false);
    }

    void SumarPunto()
    {
        puntosTexto.text = (++puntos).ToString();
        if (puntos >= GetMaximaPuntuacion())
        {
            nuevoRecord = true;
            puntosRecordTexto.text = "BEST: " + puntos.ToString();
            SetMaximaPuntuacion(puntos);
        }
    }

    public int GetMaximaPuntuacion()
    {
        return PlayerPrefs.GetInt("MaxPoints", 0);
    }

    public void SetMaximaPuntuacion(int puntosPartida)
    {
        PlayerPrefs.SetInt("MaxPoints", puntosPartida);
    }

    void Parallax()
    {
        float velocidadFinal = velocidadParallax * Time.deltaTime;
        fondo.uvRect = new Rect(fondo.uvRect.x + velocidadFinal, 0f, 1f, 1f);
        plataforma.uvRect = new Rect(plataforma.uvRect.x + velocidadFinal * 4, 0f, 1f, 1f);
    }

    void ParallaxEnvenenado()
    {
        float velocidadFinal = 8 * velocidadParallax * Time.deltaTime;
        fondo.uvRect = new Rect(fondo.uvRect.x + velocidadFinal * 8, 0f, 1f, 1f);
        plataforma.uvRect = new Rect(plataforma.uvRect.x + velocidadFinal * 4, 0f, 1f, 1f);
    }
    #endregion

    void RecibirRecords( Dictionary<string, string> records)
    {
        var Records = records;
    }
}
