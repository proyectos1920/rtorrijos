﻿using UnityEngine;
using System.Data;
using Mono.Data.Sqlite;
using System.IO;
using System.Collections.Generic;
using System;

public class DataBase : MonoBehaviour
{
    private const string stringConexion = @"URI=file:C:\Users\SYKLOS\ProyectoGS\Assets\DB\its_madness.db";

	private string IdJugador, puntos, enemigosEliminados, muertesPrevias, saltosRealizados, nombre;
	private DateTime Fecha;
	public GameObject juego;

	// Use this for initialization
	void Start()
	{

	}

	// Update is called once per frame
	void Update()
	{

	}

	void InsertarRecord(string nombre, string puntos)
    {
		// Abre conexion
		SqliteConnection dbcon = new SqliteConnection(stringConexion);
		dbcon.Open();
		// Inserta valores en la tabla
		SqliteCommand cmnd = dbcon.CreateCommand();
		cmnd.CommandText = "INSERT INTO Jugador (Nombre, Fecha) VALUES (" + nombre + ", " + Fecha.ToString() + ")";
		cmnd.CommandText = "INSERT INTO Estadisticas (EnemigosEliminados, MuertesPrevias, SaltosRealizados, Puntuacion)" +
			" VALUES ("+enemigosEliminados.ToString() + "," + muertesPrevias.ToString() + "," + saltosRealizados.ToString() + ", " + puntos.ToString() + ")";
		cmnd.ExecuteNonQuery();
		// Cerrar conexíon
		dbcon.Close();
	}

	void LeerRecord()
    {
		List<string> puntos = new List<string>(), enemigosEliminados = new List<string>(), muertesPrevias = new List<string>(), saltosRealizados = new List<string>(), nombre = new List<string>(), fecha = new List<string>();

		List<List<string>> listasEstadisticas = new List<List<string>>();
		List<List<string>> listasJugador = new List<List<string>>();

		// Abre conexion
		SqliteConnection dbcon = new SqliteConnection(stringConexion);
		dbcon.Open();

		// Lee valores
		SqliteCommand cmnd_read = dbcon.CreateCommand();
		SqliteDataReader reader;
		string query = "SELECT * FROM Estadisticas";
		cmnd_read.CommandText = query;
		cmnd_read.ExecuteNonQuery();
		reader = cmnd_read.ExecuteReader();


		while (reader.Read())
		{
			while (reader.HasRows)
            {
				puntos.Add(reader.Read().ToString());
				if (reader.NextResult())
                {
					while (reader.HasRows)
					{
						enemigosEliminados.Add(reader.Read().ToString());
						if(reader.NextResult())
                        {
							while (reader.HasRows)
							{
								muertesPrevias.Add(reader.Read().ToString());
								if(reader.NextResult())
                                {
									while (reader.HasRows)
									{
										saltosRealizados.Add(reader.Read().ToString());
									}
								}
							}
						}
					}
				}
			}
		}

		string queryJ = "SELECT * FROM Jugadores";
		cmnd_read.CommandText = queryJ;
		cmnd_read.ExecuteNonQuery();
		reader = cmnd_read.ExecuteReader();


		while (reader.Read())
		{
			while (reader.HasRows)
            {
				nombre.Add(reader.Read().ToString());
				if (reader.NextResult())
                {
					while(reader.HasRows)
                    {
						fecha.Add(reader.Read().ToString());
                    }
                }
            }
		}

		// Cierra conexion
		dbcon.Close();

		listasEstadisticas.Add(puntos);
		listasEstadisticas.Add(enemigosEliminados);
		listasEstadisticas.Add(muertesPrevias);
		listasEstadisticas.Add(saltosRealizados);
		listasJugador.Add(nombre);
		listasJugador.Add(fecha);

		// Manda valores
		juego.SendMessage("RecibirEstadisticas", listasEstadisticas);
		juego.SendMessage("RecibirJugador", listasJugador);
	}
}