﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class EnemigoController : MonoBehaviour
{

    public float velocity;
    public Rigidbody2D rb2d;
    public Animator animator;
    public SpriteRenderer sprite;

    // Start is called before the first frame update
    void Start()
    {
        sprite = GetComponentInChildren<SpriteRenderer>();
        rb2d = GetComponent<Rigidbody2D>();
        rb2d.velocity = Vector2.left * velocity;
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Destructor")
        {
            Destroy(gameObject);
        }
        if (other.gameObject.tag == "JugadorGrande")
        {
            animator.enabled = false;
            rb2d.velocity = Vector2.right * velocity;
            Invoke("Cielo", 0.25f);
            sprite.color = Color.black;
        }
    }

    void Cielo()
    {
        rb2d.velocity = Vector2.up * velocity / 2f;
    }
}
