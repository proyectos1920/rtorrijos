﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneradorEnemigosController : MonoBehaviour
{
    //Objetos
    public GameObject juego;

    //Prefabs
    public GameObject enemigoPrefab;
    public GameObject fuegoPrefab;
    public GameObject setaPrefab;
    public GameObject setaEnvenenadaPrefab;

    //Audio
    public AudioClip fuegoLanza;
    private AudioSource audioPlayer;

    //Variables
    public float tiempoReaparicion;
    int contadorEnemigos;
    Vector3 posicion;


    // Start is called before the first frame update
    void Start()
    {
        audioPlayer = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void Awake()
    {

    }

    public void CreacionEnemigos()
    {
        contadorEnemigos++;

        float tiempoRandom = GeneradorRandom();
        bool jugando = juego.GetComponent<GameController>().estadoPartida == EstadoPartida.Jugando;
        bool grande = juego.GetComponent<GameController>().estadoPartida == EstadoPartida.Grande;
        posicion.x = GeneradorRandom();
        //Posición para randomizar la salida de enemigos
        Invoke("CreateEnemigo", 0f);

        //Lógica para correcta jugabilidad
        if (contadorEnemigos > tiempoRandom - 17 && jugando)
        {
            contadorEnemigos = 0;
            if (grande) { GeneradorBoost(); }
            //Fuego
            Invoke("CreateFuego", 2.5f);

            if(Time.timeScale > 1)
            {
                Invoke("CreateEnemigo", 1.5f);
                Invoke("CreateEnemigo", 1.65f);
            }
        }

        if (tiempoRandom > 18 && jugando)
        {
            //Seta
            Invoke("CreateSeta", 0f);
        }

        if (tiempoRandom > 12 && tiempoRandom < 18 && jugando)
        {
            //SetaEnvenenada
            Invoke("CreateSetaEnvenenada", 0f);
        }
    }

    //Creates
    #region MetodosCreacion
    public void CreateFuego()
    {
        //Creado conforme la posición del enemigo para que no se junten: posicion.x
        posicion = new Vector3(posicion.x, fuegoPrefab.transform.position.y, 0);
        Instantiate(fuegoPrefab, posicion, Quaternion.identity);
        audioPlayer.clip = fuegoLanza;
        audioPlayer.Play();
    }

    public void CreateSeta()
    {
        Instantiate(setaPrefab, setaPrefab.transform.position, Quaternion.identity);
    }

    public void CreateSetaEnvenenada()
    {
        Instantiate(setaEnvenenadaPrefab, setaEnvenenadaPrefab.transform.position, Quaternion.identity);
    }

    public void CreateEnemigo()
    {
        posicion = new Vector3(posicion.x, enemigoPrefab.transform.position.y, 0);
        Instantiate(enemigoPrefab, posicion, Quaternion.identity);
    }

    public void GeneradorBoost()
    {
        Invoke("CreateEnemigo", 0f);
        Invoke("CreateFuego", 0.5f);
        Invoke("CreateEnemigo", 1f);
        Invoke("CreateFuego", 2.5f);
        Invoke("CreateEnemigo", 2f);
        Invoke("CreateFuego", 3.5f);
    }
    #endregion

    //Start/Stop
    #region Metodos generador
    public void StartGenerator()
    {
        InvokeRepeating("CreacionEnemigos", 0f, tiempoReaparicion);
    }

    public void StopGenerator(bool limpiar = false)
    {
        CancelInvoke("CreacionEnemigos");
        //CancelInvoke("CreateFuego");
        if (limpiar)
        {
            Object[] enemigos = GameObject.FindGameObjectsWithTag("Enemigo");
            foreach (GameObject enemigo in enemigos)
            {
                Destroy(enemigo);
            }
            Object[] fuegos = GameObject.FindGameObjectsWithTag("Fuego");
            foreach (GameObject fuego in fuegos)
            {
                Destroy(fuego);
            }
        }
    }
    #endregion

    //TODO: Borrar
    //RandomGenerator
    #region MetodosPrivados
    public float GeneradorRandom()
    {
        //Sumamos y multiplicamos ya que el valor oscila entre 0-1 y es muy poco tiempo.
        //Valor 10-20
        return (Random.value + 1) * 10;
    }
    #endregion
}
