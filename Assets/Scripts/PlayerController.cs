﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    //Objetos
    public GameObject juego;
    public GameObject generadorEnemigos;
    private Animator animator;
    public SpriteRenderer spriteRenderer;
    
    //Prefabs
    public ParticleSystem particulas;

    //Variables
    private float posicionInicial;
    public float tiempoInicial;
    private Color colorInicial;
        //Booleans
    private bool enJuego;
    private bool envenenado;
    private bool grande;
    private bool jugando;

    //Audios
    public AudioClip sonidoSalta;
    public AudioClip sonidoJugadorMuere;
    public AudioClip sonidoGrandePequeño;
    public AudioClip punto;
    public AudioClip fuegoChoca;
    private AudioSource audioPlayer;


    // Start is called before the first frame update
    void Start()
    {
        //Objetos
        animator = GetComponent<Animator>();
        audioPlayer = GetComponent<AudioSource>();

        //Variables
        posicionInicial = transform.position.y;
        tiempoInicial = Time.timeScale;
        colorInicial = spriteRenderer.color;
    }

    // Update is called once per frame
    void Update()
    {
        bool tocandoSuelo = transform.position.y == posicionInicial;
        bool accionJugador = Input.GetKeyDown("up") || Input.GetMouseButtonDown(0);

        enJuego = juego.GetComponent<GameController>().estadoPartida == EstadoPartida.Jugando;
        grande = juego.GetComponent<GameController>().estadoPartida == EstadoPartida.Grande;
        envenenado = juego.GetComponent<GameController>().estadoPartida == EstadoPartida.Envenenado;
        jugando = enJuego || grande || envenenado;

        if (jugando && accionJugador && tocandoSuelo)
        {
            Salta();
        }
    }

    public void ActualizaEstado(string estado = null)
    {
        if (estado != null)
        {
            animator.Play(estado);
        }
    }

    void OnTriggerEnter2D(Collider2D colliderAjeno)
    {
        //Colisiones
        if (colliderAjeno.gameObject.tag == "Enemigo" && jugando)
        {
            if (grande)
            {
                juego.SendMessage("SumarPunto");
            }
            else { MuerteJugador(); }
        }
        if (colliderAjeno.gameObject.tag == "Fuego" && jugando)
        {
            if (grande) 
            {
                juego.SendMessage("SumarPunto");
                EfectoSonido(fuegoChoca);
            }
            else { MuerteJugador(); }
        }
        if (colliderAjeno.gameObject.tag == "Seta" && jugando)
        {
            juego.SendMessage("SumarPunto");
            if (!grande && !envenenado)
            {
                transform.position = new Vector3(transform.position.x, -3.35f);
                EngrandecerJugador();
                juego.GetComponent<GameController>().estadoPartida = EstadoPartida.Grande;
            }
        }
        if (colliderAjeno.gameObject.tag == "SetaVenenosa" && jugando)
        {
            if (!grande && !envenenado)
            {
                spriteRenderer.color = Color.green;
                juego.GetComponent<GameController>().estadoPartida = EstadoPartida.Envenenado;
                Invoke("Jugando", 8f);
            }
        }
        if (colliderAjeno.gameObject.tag == "Punto" && jugando)
        {
            EfectoSonido(punto);
            juego.SendMessage("SumarPunto");
        }
    }

    //Saltar, reducir/engrandecer y muerte
    #region Métodos de acción
    void Salta()
    {
        if (envenenado)
        {
            EfectoSonido(sonidoSalta);
            ActualizaEstado("JugadorSaltaEnvenenado");
        }
        else
        {
            if (!grande) 
            {
                EfectoSonido(sonidoSalta);
                ActualizaEstado("JugadorSalta"); 
            }
        }
    }
    void ReducirJugador()
    {
        EfectoSonido(sonidoGrandePequeño);
        ActualizaEstado("JugadorPequeño");
        juego.SendMessage("RalentizaTiempo");
    }

    void EngrandecerJugador()
    {
        //TODO: SonidoGrande y abajo sonidoPequeño
        audioPlayer.volume *= 2;
        EfectoSonido(sonidoGrandePequeño);
        //Lógica
        ActualizaEstado("JugadorGrande");
        //sonido mario
        Invoke("ReducirJugador", 10f);

        juego.SendMessage("RalentizaTiempo");
        generadorEnemigos.SendMessage("GeneradorBoost");
    }

    void MuerteJugador()
    {
        //Lógica
        ActualizaEstado("JugadorMuere");
        generadorEnemigos.SendMessage("StopGenerator", true);
        juego.GetComponent<GameController>().estadoPartida = EstadoPartida.Acabado;

        //Audio
        juego.GetComponent<AudioSource>().Stop();
        EfectoSonido(sonidoJugadorMuere);
    }

    #endregion

    //Acabar, particulas y tiempo
    #region Metodos de animaciones 
    //Llamado por animación de muerte
    void AcabarPartida()
    {
        juego.GetComponent<GameController>().estadoPartida = EstadoPartida.Acabado;
    }
    //Llamadas por animación grande y jugadorCorre
    #region Partículas
    void ParticulasEmpiezan()
    {
        particulas.Play();
    }

    void ParticulasParan()
    {
        particulas.Stop();
    }
    #endregion

    //Llamado por animacion de JugadorPequeño
    void TiempoNormal()
    {
        juego.SendMessage("NormalizaTiempo");
    }
    #endregion

    //Sonido y jugando
    #region Metodos complementarios
    void Jugando()
    {
        spriteRenderer.color = colorInicial;
        juego.GetComponent<GameController>().estadoPartida = EstadoPartida.Jugando;
        audioPlayer.volume /= 2;
    }

    void EfectoSonido(AudioClip clip)
    {
        audioPlayer.clip = clip;
        audioPlayer.Play();
    }
    #endregion
}
